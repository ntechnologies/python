from webassets import Bundle
import os.path
from jinja2 import Environment as Jinja2Environment
from webassets import Environment as AssetsEnvironment
from webassets.ext.jinja2 import AssetsExtension, Jinja2Loader
from webassets.filter.jinja2 import Jinja2

# Wrapper class for webassets static content generator library
# Implements custom configuration parsing (as read from yml)
# See README for more information on configuration format
class Builder:
    def __init__(self, config, url, src_path, dst_path):
        self.config = config
        self.src_path = src_path
        self.dst_path = dst_path
        self.env = AssetsEnvironment(directory=dst_path, url=url, load_path=[src_path])
        
        jinja2_env = Jinja2Environment(extensions=[AssetsExtension])
        jinja2_env.assets_environment = self.env

        # This monkey patch is here to supply Jinja2 environment to filter for webassets
        # This issue will be solved in webassets 0.9, it should be removed after updating version
        def output_with_env(self, _in, out, **kw):
            out.write(jinja2_env.from_string(_in.read(), template_class = self.jinja2.Template).render(self.context or {}))

        Jinja2.output = output_with_env

    def parse_bundle(self, name, outname, path, content, filters):
        outpath = '%s/%s' % (path, outname)
        bundle = Bundle(*content, output=outpath, filters=filters)
        bundle_name = '%s/%s' % (path, name)
        self.env.register(bundle_name, bundle)
        output = self.env[bundle_name].urls()
        print "Processing bundle: %s, output: %s" % (bundle_name, output[0])
        for f in content:
            if type(f) is Bundle:
                print "  Included file (template): %s" % f.contents[0]
            else:
                print "  Included file: %s" % f


    def bundle_jinja(self, path):
        bundle = Bundle(path, filters='jinja2')
        self.env.register(path, bundle)
        print "Processing template: %s" % path
        return bundle

    def parse_content(self, content):
        output = []
        for el in content:
            if type(el) is dict:
                if el['type'] == 'jinja':
                    output.append(self.bundle_jinja(el['name']))
                else:
                    output.append(el['name'])
            else:
                output.append(el)
        return output

    def bundle_directory(self, outname, content, cache):
        for directory in content:
            print "Processing directory: %s" % directory
            src_dir = '%s/%s' % (self.src_path, directory)
            for (path, dirs, files) in os.walk(src_dir):
                for el in files:
                    rel_path = os.path.relpath(path, src_dir)
                    bundle_name = os.path.normpath('%s/%s/%s' % (outname, rel_path, el))

                    if cache:
                        cache_busted_filepath = '%s/%s/%s-%%(version)s.%s' % ((outname, rel_path) + tuple(el.split('.')))
                        outpath = cache_busted_filepath
                    else:
                        outpath = bundle_name
                        
                    filepath = '%s/%s/%s' % (directory, rel_path, el)
                    bundle = Bundle(filepath, output=outpath)
                    self.env.register(bundle_name, bundle)
                    output = self.env[bundle_name].urls()
                    print "  Processing file: %s, output: %s" % (os.path.normpath(filepath), output[0]) 

    def build(self):
        for bundle in self.config:
            content = self.parse_content(bundle['content'])
            if bundle['type'] == 'js' or bundle['type'] == 'css' or bundle['type'] == 'plain':
                if bundle['cache_buster']:
                    outname = '%s-%%(version)s.%s' % tuple(bundle['name'].split('.'))
                else:
                    outname = bundle['name']

                if bundle['type'] == 'js':
                    filters = ('jsmin')
                elif bundle['type'] == 'css':
                    filters = ('cssmin')
                else:
                    filters = ()

                self.parse_bundle(bundle['name'], outname, bundle['output_path'], content, filters)

            elif bundle['type'] == 'directory':
                self.bundle_directory(bundle['output_path'], content, bundle['cache_buster'])
                
            else:
                print "Unrecognized bundle type: %s" % bundle
